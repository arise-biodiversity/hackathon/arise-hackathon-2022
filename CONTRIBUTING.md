# Contributing

You may use this repository to store any results from your hackathon project.
Please [fork it on GitLab][1], push your change to a named branch, then send us
a pull request.

In order to keep things organized, save your results (code, documents, data,
etc.) in a new subdirectory with the name of your project or team. Your
subdirectory should at least contain a README.md file with basic information
about the results (What is it? How does it work? How do you use it? Etc.).

[1]: https://gitlab.com/arise-biodiversity/hackathon/arise-hackathon-2022/-/forks/new
