#Code for simulating camera trap data based on darwincore queries

library(RCurl)
library(WikipediR)
library(XML)
library(rvest)
library(sp)
library(suncalc)


basedir="C:\\Users\\jevans\\OneDrive - UvA\\Hackathondata"
sites=list.dirs(basedir,recursive=F,full.names=F)

for(site in sites){
	#load darwincore occurance data
	occdata=read.table(file.path(basedir,site,"darwincore","occurrence.txt",fsep="\\"),header=T,quote='',sep='\t')
	#limit to mammals and birds
	occdata2=occdata[occdata$class%in%c("Mammalia","Aves"),]
	#records per species
	occdatacount=aggregate(rep(1,nrow(occdata2))~occdata2$species,FUN="length")
	names(occdatacount)=c("species","count")


	##Get species traits
	#get wiki redirect pages for scientific names
	allwiki=lapply(occdatacount[,1],function(x){
		page_content("en","wikipedia", page_name = x)
	})
	#extract common name
	common=lapply(allwiki,function(x){xmlToDataFrame(x$parse$text$`*`)[1,2]})
	#get wiki pages
	allwiki2=lapply(common,function(x){page_content("en","wikipedia", page_name = x)})
	#get wiki pages text
	alltext=lapply(allwiki2,function(x){html_text(html_nodes(read_html(x$parse$text$`*`),'p,h1,h2,h3'))})
	alltext2=lapply(allwiki2,function(x){html_text(html_nodes(read_html(x$parse$text$`*`),'a'))})
	
	#scrape text for traits
	traits=c("nocturnal","social","solitary","crepuscular","hibernates","hibernation","Europe","European")
	status=c("Least Concern","Near Threatened","Vulnerable","Endangered","Critically Endangered","Extinct in the Wild")
	statusprobs=c(0.9,0.7,0.5,0.4,0.2,0)

	speciestraits=lapply(1:length(alltext),function(i){
		x=alltext[[i]]
		x2=alltext2[[i]]
		traitsdf=sapply(traits,function(z){
			any(unlist(lapply(x,function(y){grepl(z,y,fixed = T)})))
		})
		statusdf=sapply(status,function(z){
			any(unlist(lapply(x2,function(y){grepl(z,y,fixed = T)})))
		})

		prob=statusprobs[which(statusdf)[length(which(statusdf))]]
		if(length(prob)==0){
			prob=0
		}		

		data.frame(species=occdatacount$species[i],species2=common[[i]],t(data.frame(traitsdf)),count=occdatacount$count[i],
			prob=prob)
	})
	speciestraits=do.call(rbind,speciestraits)
	#remove hoomans
	speciestraits=speciestraits[speciestraits$species!="Homo sapiens",]
	#remove non European species
	speciestraits=speciestraits[speciestraits$Europe|speciestraits$European,]
	#remove extinct species
	speciestraits=speciestraits[speciestraits$prob>0,]	
	speciestraits$prob2=speciestraits$count/(max(speciestraits$count)+1)
	speciestraits$prob3=speciestraits$prob*speciestraits$prob2
	
	
	##Simulate deployments
	#get coords
	meta = xmlToList(file.path(basedir,site,"darwincore","metadata.xml",fsep="\\"))
	coords=strsplit(meta$dataset$abstract$para,"\n")[[1]][7]
	coords=gsub("[\\(\\)]", "", regmatches(coords, gregexpr("\\(.*?\\)", coords))[[1]])
	coords=strsplit(coords,",")[[1]]
	coords2=strsplit(coords," ")
	coords2=do.call(rbind,coords2)
	class(coords2)="numeric"		
	poly=Polygon(coords2)
	stationcoords=spsample(poly,n=5,"random")
	stationcoords=attributes(stationcoords)$coords
	deploymentstart=as.Date(strptime("2021-01-05",format="%Y-%m-%d"))
	deploymentend=as.Date(strptime("2021-7-01",format="%Y-%m-%d"))
	#data frame of deployments
	metadf=data.frame(deployment_id=paste0(site,"_",rep(1:5)),stationcoords,
		deployment_start=deploymentstart,
		device_type="wildlife camera",
		device_model="beastspotter 3000",
		site=site,
		location=paste0(site," placement ",rep(1:5))
	)
	
	#for each deployment simulate animal sightings
	alldays=seq(deploymentstart,deploymentend,1)

	#which days did some animals appear/not appear
	entrieson=rbinom(length(alldays),1,0.9)

	chosendays=alldays[entrieson==1]

	#how many entries to appear per day
	nentries=sample(1:20,sum(entrieson),replace=T)

	#at what time did these entries occur
	allentries=lapply(1:length(chosendays),function(i){
		currnentries=nentries[i]
		currdate=chosendays[i]
		suntimes=getSunlightTimes(data=data.frame(date=currdate,lat=metadf$y,lon=metadf$x))
		dayentries=lapply(1:currnentries,function(y){
			#choose a location
			locind=sample(1:5,1)
			loc=metadf[locind,]
			currsuntimes=suntimes[locind,]
			time=paste(sample(0:23,1),sample(0:59,1),sample(0:59,1),sep=":")
			currdt=strptime(paste(currdate,time),format="%Y-%m-%d %H:%M:%S")
			night=currdt<currsuntimes$dawn|currdt>currsuntimes$dusk
			winter=currdate<as.Date("2021-03-15")

			if(winter){
				species2=speciestraits[!(speciestraits$hibernates|speciestraits$hibernation),]
			}
			if(night){
				species2=species2[species2$nocturnal,]
			}else if(!night){
				species2=species2[!species2$nocturnal,]
			}
			if(nrow(species2)>0){
				currspecies=species2[sample(1:nrow(species2),1,prob=species2$prob),]
				return(data.frame(loc,dt=currdt,species=currspecies$species))
			}else{
				return(NULL)
			}
			
		})
		dayentries=do.call(rbind,dayentries)
		if(!is.null(dayentries)){
			dayentries=dayentries[order(dayentries$dt),]
			dayentries=dayentries[order(dayentries$deployment_id),]
			imagens=unlist(lapply(unique(dayentries$deployment_id),function(x){sort(sample(1:1000,nrow(dayentries[dayentries$deployment_id==x,])))}))
			dayentries$filename=paste0(paste(dayentries$deployment_id,paste0(dayentries$device_type,1),strftime(dayentries$dt,"%Y-%m-%d_%H-%M-%S"),paste0("(",imagens,")"),sep="_"),".jpg")
			return(dayentries)
		}

	})
	allentries=do.call(rbind,allentries)
	write.csv(allentries,file.path(basedir,site,paste0(site,"_classified_image_data.csv"),fsep="\\"))
	
}